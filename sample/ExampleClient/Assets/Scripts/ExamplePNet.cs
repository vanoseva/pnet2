using System;
using Lidgren.Network;
using PNet;
using Debug = UnityEngine.Debug;
using UnityEngine;
using Net = PNetU.Net;

public class ExamplePNet : MonoBehaviour
{

    public string ip = "127.0.0.1";
    public int port = 14000;

    private static ExamplePNet _singleton;

    void Awake()
    {
        //singleton behaviour, to prevent multiple event subscriptions/connectings
        if (_singleton)
        {
            Destroy(this);
            return;
        }

        _singleton = this;
        DontDestroyOnLoad(this);

        Net.OnRoomChange += OnRoomChange;
        Net.OnFailedToConnect += NetOnOnFailedToConnect;
        Net.OnDisconnectedFromServer += OnDisconnectedFromServer;
    }

    private void NetOnOnFailedToConnect(string s)
    {
        Debug.LogError("Failed to connect. " + s);
    }

    private void OnDisconnectedFromServer()
    {
        Debug.Log("Disconnected from server");
    }

    private void OnRoomChange(string s)
    {
        Debug.Log("server switched us to room " + s);

        //TODO: this should probably be called after we actually switch scenes, but because we're not changing scenes, we'll call it right now
        Net.FinishedRoomChange();

        //Net.RPC(3, StringSerializer.Instance.Update("This is a static rpc call. It goes to a room behaviour that is subscribed to it."));
    }

    private void OnDestroy()
    {
        //This is required for cleanup, as unity can't clean up delegates very well
        Net.OnRoomChange -= OnRoomChange;
        Net.OnFailedToConnect -= NetOnOnFailedToConnect;
        Net.OnDisconnectedFromServer -= OnDisconnectedFromServer;
    }

    // Use this for initialization
	void Start ()
	{
        var config = new PNetC.NetworkConfiguration(serverAddress: ip, serverPort:port);
        Debug.Log("connecting to " + ip + ":" + port);
	    Net.Connect(config);
	}
}