using System;
using Lidgren.Network;
using PNet;
using PNetU;
using UnityEngine;

/* ONE OF RPC USAGE

     // In a common file between C/S
    [NetComponent(1)]
    interface SampleRPC
    {
        public const byte ServerFoo = 0;
        public const byte ClientBar = 1;
    }

    // Server Side Impl
    public class SampleServer : Some, SampleRPC
    {
        [Rpc(SampleRPC.ServerFoo)]
        void ServerFoo(...) {}
    }

    // Client Side Impl
    public class SampleClient : NetBehaviour, SampleRPC
    {
        [Rpc(SampleRPC.ClientBar)]
        void ClientBar(...) {}
    }

    Server -> Cient
    NetView.Rpc<SampleRPC>(SampleRPC.ClientBar, PNetR.Player, ...);
 
    Client -> Server
    NetView.Rpc<SampleRPC>(SampleRPC.ServerFoo, RPCMode, ...);

    Client -> Client
    NetView.Rpc<SampleRPC>(SampleRPC.ClientBar, RPCMode, ...);
*/

public class NetPlayer : NetBehaviour
{
    [SerializeField]
    private MonoBehaviour[] _behavioursToDisableIfNotMine = null;
    [SerializeField]
    private GameObject[] _objectsToDisableIfNotMine = null;

    private void OnDeserializeStream(NetMessage netIncomingMessage)
    {        
        //deserialize position from the stream
        //TODO: implement smoothing/lag compensation
        transform.position = transform.position.Deserialize(netIncomingMessage);
        transform.rotation = transform.rotation.Deserialize(netIncomingMessage);
    }
    private void SerializeStream(NetMessage netOutgoingMessage)
    {
        //send our position to the server
        //this should only be happening on an object that is ours
        transform.position.Serialize(netOutgoingMessage);
        transform.rotation.Serialize(netOutgoingMessage);
    }

    protected override void OnFinishedCreating()
    {
        //TODO: enable/disable things because this is mine/not mine.
        if (NetView.IsMine)
        {
            Debug.Log("network view " + NetView.ViewId + " is mine", NetView);

            //The object is mine, so let's stream things to the server
            NetView.SetSerializationMethod(SerializeStream, 32);
            //turn serialization on
            NetView.StateSynchronization = UnityEngine.NetworkStateSynchronization.Unreliable;
            NetView.SerializationTime = 0.02f;
            //but do not subscribe to deserialization, as we want to ignore stuff for ourselves
        }
        else
        {
            //subscribe to the stream from the server for objects owned by others.
            NetView.OnDeserializeStream = OnDeserializeStream;

            //Anything in this array shouldn't be enabled, so disable them
            foreach (var comp in _behavioursToDisableIfNotMine)
            {
                comp.enabled = false;
            }
            foreach (var gobj in _objectsToDisableIfNotMine)
            {
                gobj.SetActive(false);
            }
        }
    }

    bool GotControl = false;

    private void Update()
    {
        if (!NetView.IsMine)
            return;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.forward * 5.0f * Time.deltaTime;
            GotControl = true;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.back * 5.0f * Time.deltaTime;
            GotControl = true;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * 5.0f * Time.deltaTime;
            GotControl = true;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * 5.0f * Time.deltaTime;
            GotControl = true;
        }

        if (!GotControl)
        {
            transform.position = new Vector3(UnityEngine.Random.Range(-5.0f, 5.0f), 0.0f, UnityEngine.Random.Range(-5.0f, 5.0f));
            transform.rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(-90.0f, 90.0f), Vector3.up);
        }
    }
}