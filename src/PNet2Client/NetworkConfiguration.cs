﻿namespace PNetC
{
    public class NetworkConfiguration
    {
        /// <summary>
        /// this should be unique per game, and should be the same on the client and server
        /// </summary>
        public readonly string AppIdentifier;

        /// <summary>
        /// address of the server
        /// </summary>
        public readonly string ServerAddress;
        /// <summary>
        /// port of the server is listening on
        /// </summary>
        public readonly int ServerPort;
        /// <summary>
        /// delete network instantiated objects on disconnection
        /// </summary>
        public readonly bool DeleteNetworkInstantiatesOnDisconnect;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appIdentifier"></param>
        /// <param name="serverAddress"></param>
        /// <param name="serverPort"></param>
        /// <param name="deleteNetworkInstantiatesOnDisconnect"></param>
        public NetworkConfiguration(string appIdentifier = "PNet", 
            string serverAddress = "localhost", int serverPort = 14000, 
            bool deleteNetworkInstantiatesOnDisconnect = true)
        {
            AppIdentifier = appIdentifier;
            ServerAddress = serverAddress;
            ServerPort = serverPort;
            DeleteNetworkInstantiatesOnDisconnect = deleteNetworkInstantiatesOnDisconnect;
        }
    }
}
