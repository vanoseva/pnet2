﻿using System;
using System.Collections.Generic;
using Lidgren.Network;
using PNet;

namespace PNetC.Impl
{
    public class LidgrenDispatchClient : ADispatchClient
    {
        internal NetClient ServerClient { get; private set; }
        private NetPeerConfiguration _serverConfiguration;

        private bool _serverShutdown;
        private int _lastFrameSize = 16;
        private int _lastServerSize = 16;

        protected internal override void Start()
        {
            _serverConfiguration = new NetPeerConfiguration(Client.Configuration.AppIdentifier);

#if DEBUG
            Debug.Log("Debug build. Simulated latency and packet loss/duplication is enabled.");
            _serverConfiguration.SimulatedLoss = 0.001f;
            _serverConfiguration.SimulatedDuplicatesChance = 0.001f;
            _serverConfiguration.SimulatedMinimumLatency = 0.1f;
            _serverConfiguration.SimulatedRandomLatency = 0.01f;
#endif

            ServerClient = new NetClient(_serverConfiguration);
            ServerClient.Start();

            NetOutgoingMessage hailMsg = null;
            if (Client.HailObject != null)
            {
                var size = Client.Serializer.SizeOf(Client.HailObject);
                var msg = NetMessage.GetMessage(size);
                Client.Serializer.Serialize(Client.HailObject, msg);

                hailMsg = ServerClient.CreateMessage(size);
                msg.Clone(hailMsg);
            }
            ServerClient.Connect(Client.Configuration.ServerAddress, Client.Configuration.ServerPort, hailMsg);
        }

        protected internal override void ReadQueue()
        {
            Client.Time = NetTime.Now;

            if (ServerClient == null) return;
            var messages = new List<NetIncomingMessage>(_lastServerSize * 2);
            _lastServerSize = ServerClient.ReadMessages(messages);

            if (_serverShutdown)
            {
                if (ServerClient.Status == NetPeerStatus.NotRunning)
                {
                    _serverShutdown = false;
                    FinalizeDisconnect();
                }
                else if (ServerClient.Status == NetPeerStatus.Running)
                {
                    ServerClient.Shutdown(Client.Server.StatusReason);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < messages.Count; i++)
            {
                var msgType = messages[i].MessageType;
                var msg = NetMessage.GetMessage(messages[i].Data.Length);
                messages[i].Clone(msg);
                msg.Sender = messages[i].SenderConnection;
                ServerClient.Recycle(messages[i]);

                if (msgType == NetIncomingMessageType.Data)
                {
                    Client.Server.ConsumeData(msg);
                }
                else if (msgType == NetIncomingMessageType.DebugMessage)
                {
                    Debug.Log(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.WarningMessage)
                {
                    Debug.LogWarning(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.StatusChanged)
                {
                    var status = (NetConnectionStatus)msg.ReadByte();
                    var statusReason = msg.ReadString();
                    var lastStatus = Client.Server.Status;
                    if (status == NetConnectionStatus.Connected)
                    {
                        var serverConn = ServerClient.ServerConnection;
                        if (serverConn == null)
                            throw new NullReferenceException("Could not get server connection after connected");
                        var remsg = serverConn.RemoteHailMessage;
                        if (remsg == null)
                            throw new NullReferenceException("Could not get player id");
                        if (remsg.RemainingBits < 16)
                            throw new Exception("Could not read player id");
                        var id = remsg.ReadUInt16();
                        ConnectedToServer(id);
                    }
                    Debug.Log("Server Status: {0}, {1}", status, statusReason);

                    try
                    {
                        if (Client.Server.Status == ConnectionStatus.Disconnected)
                        {
                            switch (lastStatus)
                            {
                                case ConnectionStatus.Disconnecting:
                                case ConnectionStatus.Connected:
                                    _serverShutdown = true;
                                    break;
                                default:
                                    RaiseFailedToConnect(statusReason);
                                    break;
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e, "[Net.Update.StatusChanged]");
                    }
                }
                else if (msgType == NetIncomingMessageType.Error)
                {
                    Debug.LogException(new Exception(msg.ReadString()) { Source = "Client.Lidgren.ReadServerMessages [Lidgren Error]" }); //this should really never happen...
                }
                else
                {
                    Debug.LogWarning("Uknown message type {0} from server", msgType);
                }

                NetMessage.RecycleMessage(msg);
            }
        }

        protected internal override void Disconnect(string reason)
        {
            ServerClient.Shutdown(reason);
        }

        protected internal override void DisconnectIfStillConnected()
        {
            ServerClient.Disconnect("");
        }

        [Obsolete("This method should not be used externally. Its purpose is for framework testing")]
        protected internal override void ServerOnlyDisconnect()
        {
            ServerClient.Disconnect("unequaldisconnect");
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected internal override void InternalSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = ServerClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            ServerClient.SendMessage(lmsg, method);
        }
    }
}
