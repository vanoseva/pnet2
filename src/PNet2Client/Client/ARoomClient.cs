﻿using System;
using System.Net;
using PNet;

namespace PNetC
{
    public abstract class ARoomClient
    {
        public enum waitForSwitchState
        {
            None,
            WaitForDc,
            WaitForRoomSwitch,
            Connecting,
        }

        protected internal Client Client;

        protected string QueuedRoomId { get; private set; }
        protected IPEndPoint QueuedRoomEndpoint { get; private set; }
        protected Guid SwitchToken { get; private set; }
        
        protected internal abstract void Start();
        protected abstract void SwitchRoom();
        protected abstract void ImplWaitForReconnect();
        protected internal abstract void ReadQueue();
        protected internal abstract void Disconnect(string reason);
        protected internal abstract NetMessage GetMessage(int size);

        public waitForSwitchState SwitchState { get; protected set; }

        internal void SwitchRoom(IPEndPoint endPoint, string roomId, Guid switchToken)
        {
            QueuedRoomId = roomId;
            QueuedRoomEndpoint = endPoint;
            SwitchToken = switchToken;
            SwitchRoom();
        }

        internal void WaitForReconnect()
        {
#if DEBUG
                Debug.Log("Waiting for reconnect");
#endif
            SwitchState = waitForSwitchState.Connecting;
            ImplWaitForReconnect();
        }

        protected void RaiseBeginRoomSwitch()
        {
            Client.RaiseBeginRoomSwitch(QueuedRoomId);
        }

        protected void RaiseFinishedRoomSwitch()
        {
            Client.RaiseFinishedRoomSwitch();
        }

        protected void RaiseFailedRoomSwitch(string reason)
        {
            Client.RaiseFailedRoomSwitch(reason);
        }

        protected void RaiseDisconnectedFromRoom(string reason)
        {
            Client.RaiseDisconnectedFromRoom(reason);
        }

        internal void InternalSendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendSceneViewMessage(msg, mode);
        }
        protected abstract void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode);

        internal void InternalSendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendViewMessage(msg, mode);
        }
        protected abstract void SendViewMessage(NetMessage msg, ReliabilityMode mode);

        internal void InternalSendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendStaticMessage(msg, mode);
        }
        protected abstract void SendStaticMessage(NetMessage msg, ReliabilityMode mode);
    }
}
