﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNetC;
using PNetR;
using PNetS;
using Room = PNetR.Room;
using Server = PNetS.Server;

namespace IntegrationTests
{
    [TestClass]
    public class SetupTest
    {
        private Server _dispatcher;
        private TestRoom _room;
        private TestClient _client;

        private PNetS.Room _dRoom;
        private PNetS.Player _dClient;
        private PNetR.Player _rClient;
        
        private static TestContext _context;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _context = context;
            Integration.SetupLoggers();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            
        }

        [TestInitialize]
        public void TestSetup()
        {
            _dispatcher = Integration.GetReadyServer();
            _room = Integration.GetConnectedRoom(_dispatcher);
            _client = Integration.GetConnectedClient(_dispatcher);

            Integration.WaitUntil(() => _dispatcher.GetPlayer(_client.Client.PlayerId) != PNetS.Player.ServerPlayer);
            _dClient = _dispatcher.GetPlayer(_client.Client.PlayerId);
            _dRoom = _dispatcher.GetRoom(_room.Room.RoomId);
            _rClient = _room.Room.GetPlayer(_client.Client.PlayerId);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _dispatcher.Shutdown();
            _room.Cleanup();
            _client.Cleanup();

            _dispatcher = null;
            _room = null;
            Thread.Sleep(100);
        }

        [TestMethod]
        public void EnsureConnected()
        {
            Assert.AreEqual(1, _dispatcher.RoomCount);
            Assert.AreEqual(ConnectionStatus.Connected, _room.Room.ServerStatus);
        }

        [TestMethod]
        public void MultipleRoomConnections()
        {
            var rooms = Integration.GetMultipleConnectedRooms();

            Assert.AreEqual(rooms.Length + 1, _dispatcher.RoomCount);

            
            //disconnect half of the rooms.
            for (int i = 0; i < rooms.Length; i++ )
            {
                if (i % 2 == 0) continue;

                rooms[i].Room.Shutdown();
            }

            for (int i = 0; i < rooms.Length; i++)
            {
                if (i % 2 == 0) continue;
                //wait up to one seconds to allow dc
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Disconnected == rooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(1);
                }

                Assert.AreEqual(ConnectionStatus.Disconnected, rooms[i].Room.ServerStatus);
            }

            Integration.WaitUntil(() => rooms.Length/2 + 1 == _dispatcher.RoomCount);

            rooms.Cleanup();
        }

        [TestMethod]
        public void DisconnectionTest()
        {
            var cl2 = Integration.GetConnectedClient(_dispatcher);
            var dp1 = _dispatcher.GetPlayer(_client.Client.PlayerId);
            var dp2 = _dispatcher.GetPlayer(cl2.Client.PlayerId);
            var dr = _dispatcher.GetRoom(_room.Room.RoomId);
            dp1.ChangeRoom(dr);
            dp2.ChangeRoom(dr);
            
            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);
            Integration.WaitUntil(() => cl2.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(cl2.Client.PlayerId) != null);

            var c1Called = false;
            var c2Called = false;
            _client.Client.Server.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c1Called = true;
            });

            cl2.Client.Server.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c2Called = true;
            });

            cl2.Client.Shutdown();
            Integration.WaitUntil(() => cl2.Client.Server.Status == ConnectionStatus.Disconnected, 4);

            dp1.PlayerRpc(3, "hello");

            Integration.WaitUntil(() => c1Called);
            Assert.IsFalse(c2Called);
        }

        [TestMethod]
        public void DispatchOnlyDisconnectTest()
        {
            
            var dp1 = _dispatcher.GetPlayer(_client.Client.PlayerId);
            dp1.NetUserData = new IntSerializer(357);
            var dr = _dispatcher.GetRoom(_room.Room.RoomId);

            _room.Room.ConstructNetData += () => new IntSerializer();

            dp1.ChangeRoom(dr);

            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);

            _client.Client.Room.SubscribeToRpc(3, message => Assert.Fail("Should not have called the rpc on _client"));

            //disconnect client 1 from only the server to screw up ids on the room
            _client.Client.TestServerOnlyDisconnect();

            Integration.WaitUntil(() => _client.Client.Server.Status == ConnectionStatus.Disconnected);
            
            //cl2 should have the id _client just had, which will screw up the room that doesn't know about the disconnected player on the dispatcher
            var cl2 = Integration.GetConnectedClient(_dispatcher);
            Integration.WaitUntil(() => _dispatcher.GetPlayer(cl2.Client.PlayerId) != PNetS.Player.ServerPlayer);
            var dp2 = _dispatcher.GetPlayer(cl2.Client.PlayerId);
            dp2.NetUserData = new IntSerializer(951);
            Assert.AreNotEqual(2, cl2.Client.PlayerId);
            Assert.IsNotNull(_room.Room.GetPlayer(1));
            dp2.ChangeRoom(dr);

            Integration.WaitUntil(() => cl2.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => 
                {
                    var pl2 = _room.Room.GetPlayer(cl2.Client.PlayerId);
                    if (pl2 == null) return false;
                    if ((pl2.NetUserData as IntSerializer).Value == 951) return true;
                    return false;
                });

            var c2Called = false;
            cl2.Client.Room.SubscribeToRpc(3, message =>
            {
                var rel = message.ReadString();
                Assert.AreEqual("hello", rel);
                c2Called = true;
            });

            var rp2 = _room.Room.GetPlayer(cl2.Client.PlayerId);
            PNetR.Debug.Log("Calling rpc to {0}", rp2);
            rp2.Rpc(3, "hello");
            Integration.WaitUntil(() => c2Called);
        }

        [TestMethod()]
        public void LoadDelayConnectTest()
        {
            _client.LoadDelay = 5000;
            var dp1 = _dispatcher.GetPlayer(_client.Client.PlayerId);
            var dr = _dispatcher.GetRoom(_room.Room.RoomId);
            dp1.ChangeRoom(dr);

            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId, 6);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);
        }
    }
}
