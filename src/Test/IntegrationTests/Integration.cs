﻿using System.Diagnostics;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNetC;
using PNetC.Impl;
using PNetR.Impl;
using PNetS;
using PNetR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PNetS.Impl;
using Player = PNetS.Player;
using Room = PNetR.Room;
using Server = PNetS.Server;

#if !LIDGRENDISPATCH
using ClientDispatchClient = PNetC.Impl.LidgrenDispatchClient;
using RoomDispatchClient = PNetR.Impl.LidgrenDispatchClient;
using DispatchServer = PNetS.Impl.LidgrenDispatchServer;
#else
using ClientDispatchClient = PNetC.Impl.TcpDispatchClient;
using RoomDispatchClient = PNetR.Impl.TcpDispatchClient;
using DispatchServer = PNetS.Impl.TcpDispatchServer;
#endif

namespace IntegrationTests
{
    static class Integration
    {
        public static void SetupLoggers()
        {
            PNetC.Debug.Logger = new TestLogger("client");
            PNetR.Debug.Logger = new TestLogger("room");
            PNetS.Debug.Logger = new TestLogger("dispatch");
        }

        public static Server GetReadyServer()
        {
            var server = new Server(new DispatchServer());
            server.VerifyPlayer += ServerOnVerifyPlayer;
            var dispatchConfig = new PNetS.NetworkConfiguration();
            server.Initialize(dispatchConfig);
            return server;
        }

        private static void ServerOnVerifyPlayer(Player player, NetMessage netMessage)
        {
            var token = netMessage.ReadString();
            Assert.AreEqual("TestToken", token);
            player.AllowConnect();
        }

        public static TestRoom GetConnectedRoom(Server server, int port = 14100)
        {
            var roomConfig = new PNetR.NetworkConfiguration(listenPort: port);
            var room = new Room(roomConfig, new LidgrenRoomServer(), new RoomDispatchClient());
            var troom = new TestRoom(room);
            room.StartConnection();

            //wait up to ten seconds to allow connection
            for (int i = 0; i < 1000; i++)
            {
                if (ConnectionStatus.Connected == room.ServerStatus)
                    break;
                Thread.Sleep(10);
            }
            Assert.AreEqual(ConnectionStatus.Connected, room.ServerStatus);

            for (int i = 0; i < 10; i++)
            {
                PNetS.Room r;
                if (server.TryGetRoom(room.RoomId, out r))
                    break;
                Thread.Sleep(10);
            }

            return troom;
        }

        public static TestRoom[] GetMultipleConnectedRooms(int count = 20, int startPort = 14101)
        {
            var trooms = new TestRoom[count];
            //start the rooms
            for (int i = 0; i < trooms.Length; i++)
            {
                var config = new PNetR.NetworkConfiguration(listenPort: startPort + i, roomIdentifier: "room" + (i % 5));
                var room = new Room(config, new LidgrenRoomServer(), new RoomDispatchClient());
                var troom = new TestRoom(room);

                room.StartConnection();
                trooms[i] = troom;
            }

            //then check for connection states
            for (int i = 0; i < trooms.Length; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Connected == trooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(1);
                }

                Assert.AreEqual(ConnectionStatus.Connected, trooms[i].Room.ServerStatus);
            }
            return trooms;
        }

        public static void Cleanup(this TestRoom[] rooms)
        {
            for (int i = 0; i < rooms.Length; i++)
            {
                if (rooms[i] == null) continue;
                rooms[i].Room.Shutdown();
            }

            //then check for connection states
            for (int i = 0; i < rooms.Length; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Disconnected == rooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(1);
                }

                Assert.AreEqual(ConnectionStatus.Disconnected, rooms[i].Room.ServerStatus);
            }
        }

        public static void WaitUntil(Func<bool> check, int timeout = 1)
        {
            var timer = new Stopwatch();
            timeout *= 1000;
            timer.Start();
            while (!check())
            {
                if (timer.ElapsedMilliseconds >= timeout)
                {
                    Assert.Fail("Timed out");
                }
                Thread.Sleep(10);
            }
        }

        public static TestClient GetConnectedClient(Server server)
        {
            var client = new Client(new LidgrenRoomClient(), new ClientDispatchClient());
            var testRig = new TestClient(client);
            var config = new PNetC.NetworkConfiguration();
            client.StartConnection(config);

            //wait up to ten seconds to allow connection
            for (int i = 0; i < 1000; i++)
            {
                if (ConnectionStatus.Connected == client.Server.Status)
                    break;
                Thread.Sleep(10);
            }
            Assert.AreEqual(ConnectionStatus.Connected, client.Server.Status);

            for (int i = 0; i < 10; i++ )
            {
                var dplayer = server.GetPlayer(client.PlayerId);
                if (dplayer.Id == client.PlayerId)
                    break;
                Thread.Sleep(10);
            }

            return testRig;
        }

        public static TestClient GetClient()
        {
            var client = new Client(new LidgrenRoomClient(), new ClientDispatchClient());
            var testRig = new TestClient(client);
            return testRig;
        }
    }
}
