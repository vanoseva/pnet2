using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNet.Structs;
using PNetC;

namespace IntegrationTests
{
    internal class TestClient
    {
        public int LoadDelay = 0;
        public readonly Client Client;
        public TestClient(Client client)
        {
            Client = client;
            Client.HailObject = new StringSerializer("TestToken");
            Client.BeginRoomSwitch += ClientOnBeginRoomSwitch;
            Client.NetworkManager.ViewInstantiated += NetworkManagerOnViewInstantiated;
            var runThread = new Thread(Loop);
            runThread.Start();
        }

        private void NetworkManagerOnViewInstantiated(NetworkView instance, Vector3F position, Vector3F rotation)
        {
            Debug.Log("Instantiated {0} at {1} {2}", instance, position, rotation);
        }

        private async void ClientOnBeginRoomSwitch(string s)
        {
            await Task.Delay(LoadDelay);
            Client.FinishRoomSwitch();
        }

        private bool quit = false;
        private void Loop()
        {
            while (!quit)
            {
                Client.ReadQueue();
                Thread.Sleep(1);
            }
        }

        /// <summary>
        /// connect this test client, and assert that it is connected (blocking)
        /// </summary>
        public PNetS.Player Connect(PNetS.Server server)
        {
            var config = new NetworkConfiguration();
            Client.StartConnection(config);

            //wait up to ten seconds to allow connection
            for (int i = 0; i < 1000; i++)
            {
                if (ConnectionStatus.Connected == Client.Server.Status)
                    break;
                Thread.Sleep(10);
            }
            Assert.AreEqual(ConnectionStatus.Connected, Client.Server.Status);

            for (int i = 0; i < 50; i++)
            {
                var dplayer = server.GetPlayer(Client.PlayerId);
                if (dplayer.Id == Client.PlayerId)
                    return dplayer;
                Thread.Sleep(10);
            }
            return null;
        }

        public void Cleanup()
        {
            Client.Shutdown();
            Client.Cleanup();

            for (int j = 0; j < 1000; j++)
            {
                if (ConnectionStatus.Disconnected == Client.Server.Status)
                    break;
                Thread.Sleep(1);
            }
        }
    }
}