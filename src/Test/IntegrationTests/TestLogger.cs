﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;

namespace IntegrationTests
{
    /// <summary>
    /// Console recipient for the log
    /// </summary>
    public sealed class TestLogger : ILogger
    {
        private readonly string _name;

        public TestLogger(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Info(string info, params object[] args)
        {
            Console.WriteLine(_name + ": " + info, args);
        }

        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Warning(string info, params object[] args)
        {
            Console.WriteLine(_name + ": " + info, args);
        }

        /// <summary>
        /// error
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        public void Error(string info, params object[] args)
        {
            Console.WriteLine(_name + ": ERROR " + info, args);
            Assert.Fail(info, args);
        }

        public void Exception(Exception exception, string info, params object[] args)
        {
            Console.WriteLine(_name + ": EXCEPTION {1} : {0}", exception, string.Format(info, args));
            Assert.Fail(exception + info, args);
        }
    }
}
