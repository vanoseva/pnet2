﻿using UnityEngine;
using UnityEditor;

namespace PNetU.Editor
{
    [CustomEditor(typeof(EngineHook))]
    class HookEditor : UnityEditor.Editor
    {
        private bool _foldState;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var hook = target as EngineHook;
            if (hook == null) return;

            _foldState = EditorGUILayout.Foldout(_foldState, "Networked Objects");
            if (_foldState)
            {
                EditorGUI.indentLevel++;
                foreach (var kvp in hook.Manager.Items)
                {
                    EditorGUILayout.ObjectField(kvp.Key.Id.ToString(), kvp.Value.gameObject, typeof (GameObject), true);
                }
                EditorGUI.indentLevel--;
            }
        }
    }
}
