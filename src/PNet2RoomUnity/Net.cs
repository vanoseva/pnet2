﻿using System;
using System.Management.Instrumentation;
using PNet;
using PNetR;
using PNetR.Impl;
using UnityEngine;

namespace PNet2RoomUnity
{
    public class Net : MonoBehaviour
    {
        [Serializable]
        public class NetConfiguration
        {
            public int Port = 14100;
            public int MaxPlayers;
            public int DispatchPort = 14001;
            public string DispatchAddress = "localhost";
            public string AppIdentifier = "PNet";
        }

        public NetConfiguration Configuration;

        public ConnectionStatus DispatchStatus;

        public Room Room { get; private set; }

        internal NetworkViewManager Manager { get; private set; }

        void Awake()
        {
            Room =
                new Room(
                    new NetworkConfiguration(Configuration.MaxPlayers, Configuration.Port,
                        roomIdentifier: Application.loadedLevelName,
                        dispatcherAddress: Configuration.DispatchAddress, dispatcherPort: Configuration.DispatchPort,
                        appIdentifier: Configuration.AppIdentifier),
                    new LidgrenRoomServer(),
                    new LidgrenDispatchClient());

            Manager = new NetworkViewManager(this);
        }

        void Update()
        {
            Room.ReadQueue();
            DispatchStatus = Room.ServerStatus;
        }

        void OnDestroy()
        {
            Room.Shutdown();
        }

        public void StartConnection()
        {
            Room.StartConnection();
        }

        #region events
        public event Action<Player> PlayerAdded
        {
            add { Room.PlayerAdded += value; }
            remove { Room.PlayerAdded -= value; }
        }

        public event Action<Player> PlayerRemoved
        {
            add { Room.PlayerRemoved += value; }
            remove { Room.PlayerRemoved -= value; }
        }

        public event Func<INetSerializable> ConstructNetData
        {
            add { Room.ConstructNetData += value; }
            remove { Room.ConstructNetData -= value; }
        }
        #endregion

        /// <summary>
        /// Instantiate the resource at the specified path over the network, with the specified owner
        /// </summary>
        /// <param name="path">path to the resource. Identical to the argument for UnityEngine.Resources.Load(string path). Must exist on both client and server</param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <param name="owner">owner of the object. Null is the server.</param>
        public void NetworkInstantiate(string path, Vector3 position, Quaternion rotation, Player owner = null)
        {
            var resource = Resources.Load(path, typeof(GameObject)) as GameObject;
            if (resource == null)
                throw new InstanceNotFoundException("Could not find GameObject resource in Resources/" + path);

            var gobj = Instantiate(resource, position, rotation) as GameObject;
            var view = gobj.GetComponent<NetworkView>();
            var rview = Room.Instantiate(path, position.ToPNet(), rotation.eulerAngles.ToPNet(), owner);
            
            Manager.AddView(rview, view);
        }

        public bool TryGetView(NetworkViewId id, out NetworkView view)
        {
            return Manager.TryGetView(id, out view);
        }
    }
}
