﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet.Structs;
using UnityEngine;

namespace PNet2RoomUnity
{
    public static class UnityExtensions
    {
        public static Vector3F ToPNet(this Vector3 vector)
        {
            return new Vector3F(vector.x, vector.y, vector.z);
        }
    }
}
