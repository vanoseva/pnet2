﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using PNet;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace PNetU
{
    /// <summary>
    /// network hooking into the Update method of unity
    /// </summary>
    internal class EngineHook : MonoBehaviour
    {
        readonly NetworkViewManager _manager = new NetworkViewManager();
        internal NetworkViewManager Manager { get { return _manager; } }

        static EngineHook _instance;
        //because we're dontdestroyonload, if we're ever destroyed
        //we shouldn't attempt to recreate an instance if requested
        //because someone might be erronously calling us.
        static bool _destroyed = false;
        public static EngineHook Instance
        {
            get
            {
                if (_instance == null && !_destroyed)
                {
                    var gobj = new GameObject("PNetU Singleton Engine Hook");
                    _instance = gobj.AddComponent<EngineHook>();
                    //gobj.hideFlags = HideFlags.DontSave;
                    DontDestroyOnLoad(gobj);
                }
                return _instance;
            }
        }
        /// <summary>
        /// if the instance is valid. If this is not used, calls to Instance can cause unity to leak objects
        /// </summary>
        public static bool ValidInstance
        {
            get
            {
                if (_instance != null || (_instance == null && !_destroyed))
                    return true;
                return false;
            }
        }

        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public string StatusReason;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public ConnectionStatus Status;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public string RoomStatusReason;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public ConnectionStatus RoomStatus;

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                //we're probably creating a new object after having explicitly destroyed, so reset.
                _destroyed = false;
                DontDestroyOnLoad(this);
            }

            if (_instance != this)
                Destroy(this);

            PNetC.Debug.Logger = gameObject.AddComponent<UnityDebugLogger>();

            Net.Peer.NetworkManager.ViewInstantiated += Instantiate;
        }

        /// <summary>
        /// Run every frame, as long as the script is enabled
        /// </summary>
        [UsedImplicitly]
        void Update()
        {
            Net.Peer.ReadQueue();
            StatusReason = Net.StatusReason;
            Status = Net.Status;
            RoomStatusReason = Net.Peer.Room.StatusReason;
            RoomStatus = Net.Peer.Room.Status;
        }

        [UsedImplicitly]
        void OnDestroy()
        {
            if (_instance == this)
            {
                _destroyed = true;
                Net.Disconnect();
                //run some cleanup too, just in case
                Net.CleanupEvents();
                Manager.Clear();
            }
        }

        internal static Dictionary<string, GameObject> ResourceCache = new Dictionary<string, GameObject>();

        void Instantiate(PNetC.NetworkView newView, PNet.Structs.Vector3F location, PNet.Structs.Vector3F rotation)
        {
            GameObject gobj;
            bool isCached = false;
            if (Net.ResourceCaching && (isCached = ResourceCache.ContainsKey(newView.Resource)))
                gobj = ResourceCache[newView.Resource];
            else
                gobj = Resources.Load(newView.Resource) as GameObject;

            if (Net.ResourceCaching && !isCached)
                ResourceCache.Add(newView.Resource, gobj);

            Quaternion quat = Quaternion.Euler(rotation.X, rotation.Y, rotation.Z);
            if (Net.ConvertRotation != null)
                quat = Net.ConvertRotation(rotation);
            var instance = (GameObject)Instantiate(gobj, new Vector3(location.X, location.Y, location.Z), quat);

            if (instance == null)
            {
                Debug.LogWarning("could not find prefab " + newView.Resource + " to instantiate");
                instance = new GameObject("BROKEN NETWORK PREFAB " + newView.Id);
            }

            Debug.Log(string.Format("network instantiate of {0}. Loc: {1} Rot: {2}", newView.Resource, location, rotation), instance);

            //look for a networkview..
            var view = instance.GetComponent<NetworkView>();
            if (view == null)
                view = instance.AddComponent<NetworkView>();

            _manager.AddView(newView, view);

            var nBehaviours = instance.GetComponents<NetBehaviour>();

            foreach (var behave in nBehaviours)
            {
                behave.NetView = view;
            }

            view.DoOnFinishedCreation();
        }
    }
}